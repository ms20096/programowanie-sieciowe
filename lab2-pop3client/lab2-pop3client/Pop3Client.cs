﻿using System;
using System.Text;
using System.Net.Security;
using System.Net.Sockets;

namespace lab2_pop3client
{
    class Pop3Client
    {
        TcpClient m_tcpClient = new TcpClient();
        SslStream m_sslStream;
        
        byte[] m_buffer = new byte[8172];

        public bool Connect(string host, int port)
        {
            m_tcpClient.Connect(host, port);
            m_sslStream = new SslStream(m_tcpClient.GetStream());
            m_sslStream.AuthenticateAsClient(host);
            int bytes = m_sslStream.Read(m_buffer, 0, m_buffer.Length);
            return (Encoding.ASCII.GetString(m_buffer, 0, bytes).Contains("+OK"));
        }

        public void Disconnect()
        {
            m_sslStream.Close();
            m_tcpClient.Close();
        }

        public bool Login(string username, string password)
        {
            if (!m_tcpClient.Connected)
                return false;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                throw new ArgumentException("Username or Password was empty.");
            int bytesRead = -1;
            //Send the users login details
            m_sslStream.Write(Encoding.ASCII.GetBytes("USER " + username + "\r\n"));
            bytesRead = m_sslStream.Read(m_buffer, 0, m_buffer.Length);
            if (!Encoding.ASCII.GetString(m_buffer, 0, bytesRead).Contains("+OK"))
                return false;
            //Send the password                        
            m_sslStream.Write(Encoding.ASCII.GetBytes("PASS " + password + "\r\n"));
            bytesRead = m_sslStream.Read(m_buffer, 0, m_buffer.Length);
            if (!Encoding.ASCII.GetString(m_buffer, 0, bytesRead).Contains("+OK"))
                return false;
            return true;
        }

        public int GetMailCount()
        {
            int m_mailCount;
            m_sslStream.Write(Encoding.ASCII.GetBytes("STAT\r\n"));
            int bytesRead = m_sslStream.Read(m_buffer, 0, m_buffer.Length);
            string data = Encoding.ASCII.GetString(m_buffer, 0, bytesRead);
            if (data.Contains("+OK"))
            {
                data = data.Remove(0, 4);
                string[] temp = data.Split(' ');
                int r;
                if (int.TryParse(temp[0], out r))
                {
                    m_mailCount = r;
                    return r;
                }
            }
            return -1;
        }
    }
}