﻿using System;
using System.Configuration;

namespace lab2_pop3client
{
    class Program
    {
        

        static void Main(string[] args)
        {
            int mailCount = 0;
            string server = ConfigurationManager.AppSettings["server"].ToString();
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString());
            string email = ConfigurationManager.AppSettings["email"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();
            int time = Convert.ToInt32(ConfigurationManager.AppSettings["time"].ToString());

            Console.WriteLine($"Checking email count every {time} seconds. Press ESC and wait less than 10s to stop.");
            do
            {
                while (!Console.KeyAvailable)
                {
                    Pop3Client mail = new Pop3Client();
                    if (!mail.Connect(server, port))
                    {
                        Console.WriteLine($"Connect failed, attempted connection to {server} on port {port} (client).");
                        return;
                    }

                    if (!mail.Login(email, password))
                    {
                        Console.WriteLine("Login failed, valid username/pass? POP3 enabled on email settings?");
                        Console.ReadKey();
                        return;
                    }

                    Console.WriteLine("Current email count: " + mail.GetMailCount().ToString());
                    System.Threading.Thread.Sleep(time * 1000);
                    mailCount = mail.GetMailCount();

                    mail.Disconnect();
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            Console.WriteLine($"Program ended. Total number of mails: {mailCount}");
            Console.WriteLine("Press any key to exit.");
            Console.ReadLine();
        }
    }
}