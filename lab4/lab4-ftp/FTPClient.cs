ï»¿using System;
using System.Configuration;
using System.IO;
using System.Net;

namespace lab4_ftp
{
    public class FTPClient
    {
        private string host = ConfigurationManager.AppSettings["server"].ToString();
        private int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
        private string username = ConfigurationManager.AppSettings["username"].ToString();
        private string password = ConfigurationManager.AppSettings["password"].ToString();

        public void listFiles(string host, int port, string username, string password)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"ftp://{host}");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                request.Credentials = new NetworkCredential(username, password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                Console.WriteLine(names);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void menu()
        {
            Console.WriteLine("Choose what to do:");
            Console.WriteLine("1 - Print main directory");
            Console.WriteLine("2 - Print selected directory");
            Console.WriteLine("0 - Exit");

            var input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    Console.WriteLine("Main directory contains the following items: ");
                    listFiles(host, port, username, password);
                    menu();
                    break;
                case "2":
                    Console.WriteLine("Type directory name to display its content: ");
                    try
                    {
                        string dir = Console.ReadLine();
                        Console.WriteLine($"{dir} directory contains the following items: ");
                        listFiles(host + "/" + dir, port, username, password);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Something wrong with the directory name.");
                    }
                    menu();
                    break;
                case "0":
                    Console.WriteLine();
                    break;
                default:
                    Console.WriteLine("Unknown command, try again.");
                    menu();
                    break;
            }
        }
    }
}