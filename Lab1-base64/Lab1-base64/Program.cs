﻿using System;
using System.IO;

namespace Lab1_base64
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type path to file. " +
                "File with .b64 extension will be automatically, decoded." +
                "File with other extension will be encoded to .b64 file:");

            var input = Console.ReadLine();
            var path = Path.GetFullPath(input);
            var extension = Path.GetExtension(path);

            var b64encoder = new Base64Encoder();

            if (extension == ".b64")
            {
                Console.WriteLine(".b64 file detected - decoding.");
                b64encoder.DecodeFile(path);
                Console.WriteLine("Decoding completed! Press any key to exit.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("File other than .b64 detected - encoding.");
                b64encoder.EncodeFile(path);
                Console.WriteLine("Encoding completed! Press any key to exit.");
                Console.ReadLine();
            }
        }
    }
}