﻿using System;
using System.IO;

namespace Lab1_base64
{
    public class Base64Encoder
    {
        public void EncodeFile(string filePath)
        {
            byte[] bytes = File.ReadAllBytes(filePath);
            var base64string = Convert.ToBase64String(bytes);

            var fileDir = Path.GetDirectoryName(filePath);
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            var fileExtension = Path.GetExtension(filePath);

            string destFileName = Path.Combine(fileDir, fileName + ".b64");
            File.WriteAllText(destFileName, fileExtension + "|" + base64string);
        }

        public void DecodeFile(string filePath)
        {
            var str = File.ReadAllText(filePath);
            var fileExtension = str.Substring(0, str.IndexOf("|"));
            var b64string = str.Substring(str.IndexOf("|") + 1);
            var bytes = Convert.FromBase64String(b64string);

            var fileDir = Path.GetDirectoryName(filePath);
            var fileName = Path.GetFileNameWithoutExtension(filePath);

            string destFileName = Path.Combine(fileDir, fileName + fileExtension);

            using (var imageFile = new FileStream(destFileName, FileMode.Create))
            {
                imageFile.Write(bytes, 0, bytes.Length);
                imageFile.Flush();
            }
        }
    }
}