﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Text;

namespace lab3_smtp
{
    class Program
    {
        static void Main(string[] args)
        {
            string server = ConfigurationManager.AppSettings["server"].ToString();
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString());
            string emailFrom = ConfigurationManager.AppSettings["emailFrom"].ToString();
            string password = ConfigurationManager.AppSettings["password"].ToString();
            string emailTo = ConfigurationManager.AppSettings["emailTo"].ToString();
            string emailSubject = ConfigurationManager.AppSettings["emailSubject"].ToString();
            string emailBody = ConfigurationManager.AppSettings["emailBody"].ToString();

            try
            {
                MailMessage mail = new MailMessage(emailFrom, emailTo);
                SmtpClient smtpServer = new SmtpClient(server);

                mail.Subject = emailSubject;
                mail.SubjectEncoding = Encoding.UTF8;
                mail.Body = emailBody;
                mail.BodyEncoding = Encoding.UTF8;

                smtpServer.Port = port;
                smtpServer.Host = server;
                smtpServer.Timeout = 10000;
                smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpServer.EnableSsl = true;
                smtpServer.UseDefaultCredentials = false;
                smtpServer.Credentials = new System.Net.NetworkCredential(emailFrom, password);

                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                smtpServer.Send(mail);
                Console.WriteLine("E-mail has been properly sent.");
                Console.WriteLine($"Sent from {emailFrom} to {emailTo}");
                mail.Dispose();
                Console.WriteLine();
                Console.WriteLine("Press any key to exit.");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
    }
}